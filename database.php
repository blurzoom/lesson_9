<?php
/**
 * Created by PhpStorm.
 * User: blurzoom
 * Date: 26.02.2017
 * Time: 13:12
 */
require 'vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as Capsule;

//init database and table
$host = 'localhost';
$username='root';
$password='';
$nameDatabase='trainingDB';
$nameTable='students';

//sql procedure
$sql="create procedure usp_trainingDB
begin
CREATE DATABASE IF NOT EXISTS trainingDB ;
use trainingDB;
CREATE TABLE students
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR(30) NOT NULL COMMENT 'Имя',
    LastName VARCHAR(30) NOT NULL COMMENT 'Фамилия',
    Email VARCHAR(50) NOT NULL COMMENT 'Email',
    Course VARCHAR(30) NOT NULL COMMENT 'Название курса',
    Created_at DATETIME COMMENT 'дата создания',
    Updated_at DATETIME COMMENT 'дата обновления'
);
end";


//checking exist database and if not exist create
try {
    $conn = new PDO("mysql:host=$host", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "CREATE DATABASE IF NOT EXISTS $nameDatabase";
    $conn->exec($sql);
    $sql = "use $nameDatabase";
    $conn->exec($sql);
    $sql = "CREATE TABLE IF NOT EXISTS $nameTable
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR(30) NOT NULL COMMENT 'Имя',
    LastName VARCHAR(30) NOT NULL COMMENT 'Фамилия',
    Email VARCHAR(50) NOT NULL COMMENT 'Email',
    Course VARCHAR(30) NOT NULL COMMENT 'Название курса',
    Created_at DATETIME COMMENT 'дата создания',
    Updated_at DATETIME COMMENT 'дата обновления'
);";
    $conn->exec($sql);
//    echo "DB created successfully";
} catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}

$capsule = new Capsule;
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => $host,
    'database' => $nameDatabase,
    'username' => $username,
    'password' => $password,
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);
// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

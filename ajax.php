<?php
/**
 * Created by PhpStorm.
 * User: blurzoom
 * Date: 25.02.2017
 * Time: 6:11
 */
require_once 'database.php';
require_once './src/actions/add.php';
require_once './src/actions/update.php';
require_once './src/actions/delete.php';

const ADD = 1;
const UPDATE = 2;
const DELETE = 3;
const DELETE_ALL = 4;

function is_ajax_request()
{
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
}

if (is_ajax_request()) {
    if (isset($_POST['id'])) {
        $array['id'] = $_POST['id'];
    }
    if (isset($_POST['firstname'])) {
        $array['firstname'] = $_POST['firstname'];
    }
    if (isset($_POST['lastname'])) {
        $array['lastname'] = $_POST['lastname'];
    }
    if (isset($_POST['email'])) {
        $array['email'] = $_POST['email'];
    }
    if (isset($_POST['course'])) {
        $array['course'] = $_POST['course'];
    }
    if (isset($_POST['operation'])) {
        $operation = $_POST['operation'];
    }

    if ($operation == ADD) {
        echo \Database\actions\add($array);
    } elseif ($operation == UPDATE) {
        echo \Database\actions\update($array);
    } elseif ($operation == DELETE) {
        echo \Database\actions\delete($array);
    } elseif ($operation == DELETE_ALL) {
        foreach ($_POST as $key => $value) {
            if ($key != 'operation') {
                $array[] = $value;
            }
        }
//        $students::destroy($array);
//
        echo \Database\actions\deleteAll($array);
    }

} else {
    echo 'Only for ajax request';
}

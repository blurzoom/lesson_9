'use strict';
const ADD = 1;
const UPDATE = 2;
const DELETE = 3;
const DELETE_ALL = 4;
var ids = Array;
var clone;
var row = {
    id: '',
    firstName: '',
    lastName: '',
    email: '',
    course: '',
    set: function (id, input) {
        let i = 0;
        this.id = id;
        for (let prop in row) {
            if (typeof row[prop] !== 'function' && prop != 'id') {
                row[prop] = input.eq(i).val();
                i++;
            }
        }
    }
};

jQuery.fn.highlight = function (bgcolor, color, opacity) {
// if (color=='undefined')color ='#fff';
    this.css('opacity', opacity).css('background-color', bgcolor).css('color', color).css('transition', '.8s ease-in');

};
$(document).ready(function () {
    let rowHiden = $('#t-row');
    if (rowHiden.attr('data-id') == "<?php echo $value['id']?>") {
        rowHiden.attr('data-id', '-1');
        rowHiden.hide();
    } else if (rowHiden.attr('data-id') == '-1') {
        rowHiden.hide();
    }
    $('tfoot').fadeTo(1, 0.0).removeClass('fade');

    let table = $('.table');
    let trFirst = table.find('tbody tr:first');
    if (trFirst.attr('data-id') == '-1') {
        table.fadeTo(1, 0.0);
    } else {
        table.fadeTo(1000, 1.0);
    }
    table.removeClass('hidden');
});
$('#allCheckbox').click(function () {
    if ($(this).is(':checked')) {
        $('input#checkbox').prop('checked', this.checked);
        ids = $('input:checkbox:checked.group1').map(function () {
            return $(this).closest("tr").attr('data-id')
        });
        // console.log(ids);
    } else {
        $('input#checkbox').prop('checked', this.checked);
    }
});
$(document).on('click', '.btn', function () {
    let id = $(this).val();
    let tr = $('#t-row[data-id=\'' + id + '\']');
    let td = $(tr).find('td');
    let input = $('input.updateInput');
    let btn = $(this).attr('id');

    if ($(this).closest('tr').attr('data-id') !== undefined) {
        let id = $(this).closest('tr').attr('data-id');
        if (btn == 'delete') {
            $('#btnModalDelete').val(id);
            $('#modalDelete').modal();
        } else if (btn == 'update') {
            $('#btnModalUpdate').val(id);
            let td = $(this).closest('tr').find('td');
            let input = $('input.updateInput');
            for (let i = 0; i < 4; i++) {
                input.eq(i).val(td.eq(i + 1).text());
            }

            $('#modalUpdate').modal('show');
        }
    }

    if (btn == 'add') {
        var high = Math.max.apply(Math, $('tr#t-row').map(function () {
            return $(this).attr('data-id')
        }));
        $('#btnModalAdd').val(high);
        $('#formModalAdd').trigger('reset');
        $('#modalAdd').modal('show');

    } else if (btn == 'delete-all' && this.className != 'btn btn-danger disabled') {
        $('#btnModalDelete').val('deleteAll');
        $('#modalDelete').modal('show');

//    (delete row)
    } else if (btn == 'btnModalDelete') {
        deleteId($(this).val(), id, tr, td);
        $('#modalDelete').modal('hide');


// Изменение строки (edit row)
    } else if (btn == 'btnModalUpdate') {
        row.set(id, input);
        sendNewMessage(id, UPDATE);
        inputFormToTable(td, input);
        $('#modalUpdate').modal('hide');
        tr.children('td,th').highlight('#5bc0de', '#fff', 0.6);

// Добавление новой строки (add row)
    } else if (btn == 'btnModalAdd') {
        // ($('.table tbody tr').length < 2) ? tr=clone : tr;
        id = id < 0 ? 0 : id;
        clone = tr.clone();
        td = $(clone).find('td');
        clone.attr('data-id', Number(id) + 1);
        clone.removeAttr('style');
        input = $('input.addInput');
        row.set(Number(id) + 1, input);
        sendNewMessage(row.id, ADD);
        $('.table').fadeTo(1000, 1.0);
        inputFormToTable(td, input);
        $('#modalAdd').modal('hide');
        clone.insertAfter(tr);
        $('#t-row[data-id=\'' + (Number(id) + 1) + '\']').children('td,th').highlight('#449D44', '#fff', 0.6);
    }
});
$(window).on('load resize', windowSize);
$(document).on('change', 'input[type="checkbox"]', function () {
    if ($(this).is(':checked')) {
        $('#delete-all.btn.btn-danger.disabled').removeClass('disabled');
        ids = $('input:checkbox:checked.group1').map(function () {
            return $(this).closest("tr").attr('data-id')
        });
        // console.log(ids);
    } else {
        $('#delete-all.btn.btn-danger').addClass('disabled');
        ids = $('input:checkbox:checked.group1').map(function () {
            return $(this).closest("tr").attr('data-id')
        });
        // console.log(ids);
    }
});
//копирование с формы введенных данных в нужную строку таблицы
function inputFormToTable(td, input) {
    for (let i = 0; i < 4; i++) {
        td.eq(i + 1).html(input.eq(i).val());
    }
}
// отправка ajax запроса
function sendNewMessage(id, operation) {

    let data = {
        id: row.id,
        firstname: row.firstName,
        lastname: row.lastName,
        email: row.email,
        course: row.course,
        operation: operation
    };
    if (operation == 4) {
        let length = ids.length;
        data = {};
        for (let i = 0; i < length; i++) {
            data['id' + i] = ids[i];
        }
        data['operation'] = operation;
    }

    $.ajax({
        url: 'ajax.php', // Php file path
        method: 'POST', // Send data method
        data: data,
        success: (function () {
            // Alert("Успешное выполнение");

        }),
        converters: {
            "text json": jQuery.parseJSON
        },
        error: (function (response) {
            let result = JSON.parse(response.responseText);
        }),
        complete: (function (response) {
            let result = JSON.parse(response.responseText);

            if (result.hasOwnProperty('result') && result.result == 'Ok') {
                $('#result').html(result.description).closest('tfoot').stop().fadeTo(1, 0.0).fadeTo(2000, 1.0).delay(2000).fadeTo(2000, 0.0);
            } else if (result.hasOwnProperty('result') && result.result == 'Error') {
                $('#result').html(result.description).closest('tfoot').stop().fadeTo(1, 0.0).fadeTo(2000, 1.0).delay(2000).fadeTo(2000, 0.0);
                if (result.operation == ADD) {
                    $('#t-row[data-id=\'' + (result.id) + '\']').remove();
                } else if (result.operation == UPDATE) {
                    $('#t-row[data-id=\'' + (result.id) + '\']').remove();
                } else if (result.operation == DELETE) {

                } else if (result.operation == DELETE_ALL) {

                }
            }
            $('#t-row[data-id=\'' + row.id + '\']').children('td,th').highlight('#FFF', '#000', 1.0);
        }),
    });

}
// удаление строки/строк
function deleteId(btn, id, tr, td) {
    let rowCount = $('.table tbody tr').length;
    if (btn == 'deleteAll') {
        let selectedTr = $('input:checkbox:checked.group1').closest('tr');
        sendNewMessage(ids, DELETE_ALL);
        if (selectedTr.length < (rowCount - 1)) {
            selectedTr.children('td,th').highlight('#d9534f', '#fff', 0.6);
            selectedTr.delay(1700).queue(function () {
                $(this).remove();
            });

        } else {
            let row = $('.table').find('tbody tr:first').clone();
            let td = row.find('td');
            row.attr('data-id', '-1');
            for (let i = 1; i < 5; i++) {
                td.eq(i).html(null);
            }
            selectedTr.children('td,th').highlight('#d9534f', '#fff', 0.6);
            selectedTr.delay(1700).queue(function () {
                $(this).remove();
            });

            tr = $('.table').find('tbody tr:last');
            row.insertBefore(tr);
            tr = $('.table').find('tbody tr:first');
            $('#t-row[data-id = "-1"]').hide();
            $('.table').fadeTo(1700, 0.0);
        }
        $('input[type="checkbox"]').prop('checked', false);
    }
    else {
        row.id = id;
        sendNewMessage(id, DELETE);
        if (rowCount > 3) {
            tr.children('td,th').highlight('#d9534f', '#fff', 0.6);
            tr.delay(1700).queue(function () {
                $(this).remove();
            })
        } else {
            tr.children('td,th').highlight('#d9534f', '#fff', 0.6);
            tr.delay(1700).hide();
            tr.attr('data-id', '-1');
            for (let i = 1; i < 5; i++) {
                td.eq(i).html(null);
                $('.table').fadeTo(1, 0.0);
            }
        }
    }
}
function windowSize() {
    let height = $(window).height();
    let rowCount = $('.table tbody tr').length;
    // alert(height);
    let delta=(height-260) / 52;
    delta=+delta.toFixed(0);
    let calcHeight =58+35+20+40+36+50+(rowCount*52);
    if (height>calcHeight){
        $('tbody').css("height", 400);
        $('body').css("height", height);

    }else{
        $('tbody').css("height", delta*52);
        $('body').css("height", height);
        if (height<387){
            $('body').css("min-height", 386);
            $('tbody').css("height", 156);
        }
    }
}
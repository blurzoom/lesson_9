<?php
/**
 * Created by PhpStorm.
 * User: blurzoom
 * Date: 26.02.2017
 * Time: 13:32
 */

namespace Database\actions;

use Database\models\Student;

function update($array)
{
//insert into

    if (!is_null($students = Student::find($array['id']))) {
        $students->firstname = $array['firstname'];
        $students->lastname = $array['lastname'];
        $students->email = $array['email'];
        $students->course = $array['course'];
        $students->save();
        return json_encode(array(
            'result' => 'Ok',
            'operation' => 2,
            'id' => $array['id'],
            'description' => "record with id={$array['id']} updated successfully"));
    } else {
        return json_encode(array(
            'result' => 'Error',
            'operation' => 2,
            'id' => $array['id'],
            'description' => "record with id={$array['id']} has not been updated"));
    }
}
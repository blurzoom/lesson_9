<?php
/**
 * Created by PhpStorm.
 * User: blurzoom
 * Date: 26.02.2017
 * Time: 13:32
 */

namespace Database\actions;

use Database\models\Student;

function delete($array)
{
//delete
    if (is_null($students = Student::find($array['id']))) {
        return json_encode(array(
            'result' => 'Error',
            'operation' => 3,
            'id' => $array['id'],
            'description' => "record with id={$array['id']} is not found, it may be removed"));
    } else {
        $students->delete();
        return json_encode(array(
            'result' => 'Ok',
            'operation' => 3,
            'id' => $array['id'],
            'description' => "record with id={$array['id']} removed"));
    }
}

function deleteAll($array)
{
//delete All

    if (!Student::destroy($array)) {
        return json_encode(array(
            'result' => 'Error',
            'operation' => 4,
            'ids' => $array,
            'description' => "record with ids={$array} is not found, it may be removed"));
    } else {
        $array = implode($array, '; ');
        return json_encode(array(
            'result' => 'Ok',
            'operation' => 4,
            'ids' => $array,
            'description' => "records with ids={$array} removed"));
    }
}

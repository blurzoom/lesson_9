<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List Students</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap-3.3.7.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

<div class="container">
<!--    <div class="wrapper">-->

<div class="col-sm-12 clearfix">
    <header>
        <h4>Database operations with SQL: <br/>insert (add), delete,
            update(edit)</h4>
    </header>
</div>


<!--<div class="fix-height ">-->
<!--    <div class="container clearfix">-->
        <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4 col-sm-offset-1">
            <button class="btn btn-default btn-success" id="add" type="submit">
                Add &nbsp;&nbsp;<span
                        class="glyphicon glyphicon-plus-sign"></span></button>
            <button class="btn btn-danger disabled" id="delete-all"
                    type="submit">Delete &nbsp;&nbsp;<span
                        class="glyphicon glyphicon-remove-sign"></span></button>
        </div>
        <div class="col-sm-4"></div>
    </div>
        <div class="row">
        <div class="col-sm-12">
            <table class="table table-hover fixed_headers">
                <thead>
                <tr>
                    <th>
                        <div class="">
                            <input id="allCheckbox" type="checkbox" title="Select all rows">
                        </div>
                    </th>
                    <th>firstName</th>
                    <th>lastName</th>
                    <th>email</th>
                    <th>course</th>
                    <th></th>
                </tr>
                </thead>
                <tfoot class="result fade">
                <td colspan="3">Результат операций:</td>
                <td colspan="4">
                    <span id="result">record with id={$array['id']} has not added</span>
                </td>
                </tfoot>
                <tbody id="tbody-data">
                <?php

                if (count($students) === 0) {
                    unset($students);
                    $students[0]['id'] = '-1';
                    $students[0]['FirstName'] = null;
                    $students[0]['LastName'] = null;
                    $students[0]['Email'] = null;
                    $students[0]['Course'] = null;
                }
                foreach ($students as $student) {
                    ?>

                    <tr id="t-row" data-id="<?php echo $student['id'] ?>">
                        <td id="check"><input id="checkbox" class="group1" type="checkbox" title="Select this row"></td>
                        <td id="firstname"><?php echo $student['FirstName'] ?></td>
                        <td id="lastname"><?php echo $student['LastName'] ?></td>
                        <td id="email"><?php echo $student['Email'] ?></td>
                        <td id="course"><?php echo $student['Course'] ?></td>
                        <td>
                            <button style="margin-right: 20px" class="button btn btn-danger" type="submit"
                                    id="delete">Delete &nbsp;&nbsp;<span
                                        class="glyphicon glyphicon-remove-sign"></span>
                            </button>
                            <button class="button btn btn-default btn-info"
                                    type="submit" id="update">Update
                                &nbsp;&nbsp;<span
                                        class="glyphicon glyphicon-edit"></span>
                            </button>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    </div>
<div class="push clearfix"></div>
</div>
<!--</div>-->
<!--</div>-->
<!-- footer  -->
<div class="clearfix footer">
    <footer class="text-center footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <p>Copyright © MyWebsite. All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- Modal update -->
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog"
     aria-labelledby="updateModalLabel" data-backdrop="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="updateModalLabel">Редактирование</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group"><label
                                class="control-label col-sm-2 "
                                for="">FirsName: </label>
                        <div class="col-sm-10"><input class="form-control updateInput"
                                                      type="text" name=""
                                                      placeholder="First Name" value="">
                        </div>
                    </div>
                    <div class="form-group"><label
                                class="control-label col-sm-2"
                                for="">LastName: </label>
                        <div class="col-sm-10"><input class="form-control updateInput"
                                                      type="text" name=""
                                                      placeholder="Last Name" value="">
                        </div>
                    </div>
                    <div class="form-group"><label
                                class="control-label col-sm-2"
                                for="">Email: </label>
                        <div class="col-sm-10"><input class="form-control updateInput"
                                                      type="email"
                                                      placeholder="Email" value=""></div>
                    </div>
                    <div class="form-group"><label
                                class="control-label col-sm-2"
                                for="">Course: </label>
                        <div class="col-sm-10"><input class="form-control updateInput"
                                                      type="text" name=""
                                                      placeholder="course" value="">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Cancel
                </button>
                <button id="btnModalUpdate" value="" type="button"
                        class="btn btn-primary btn-info">Update
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Add -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="document"
     aria-labelledby="addModalLabel" data-backdrop="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addModalLabel">Добавление новых
                    данных</h4>
            </div>
            <div class="modal-body">
                <form id="formModalAdd" class="form-horizontal">
                    <div class="form-group"><label
                                class="control-label col-sm-2 " for="">FirsName: </label>
                        <div class="col-sm-10"><input class="form-control addInput"
                                                      type="text"
                                                      name=""
                                                      placeholder="First Name"
                                                      autocomplete="off"
                                                      value="">
                        </div>
                    </div>
                    <div class="form-group"><label
                                class="control-label col-sm-2" for="">LastName: </label>
                        <div class="col-sm-10"><input class="form-control addInput"
                                                      type="text"
                                                      name=""
                                                      placeholder="Last Name"
                                                      autocomplete="off"
                                                      value="">
                        </div>
                    </div>
                    <div class="form-group"><label
                                class="control-label col-sm-2" for="">Email: </label>
                        <div class="col-sm-10"><input class="form-control addInput"
                                                      type="email"
                                                      placeholder="Email"
                                                      autocomplete="off"
                                                      value=""></div>
                    </div>
                    <div class="form-group"><label
                                class="control-label col-sm-2" for="">Course: </label>
                        <div class="col-sm-10"><input class="form-control addInput"
                                                      type="text"
                                                      name=""
                                                      placeholder="course"
                                                      autocomplete="off"
                                                      value="">
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Cancel
                </button>
                <button id="btnModalAdd" value="" type="button"
                        class="btn btn-primary btn-success">Add
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" data-backdrop="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Попытка удалить</h4>
            </div>
            <div class="modal-body">
                <span class="info">Вы уверены, что хотите удалить ? </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Cancel
                </button>
                <button id="btnModalDelete" value="" type="button"
                        class="btn btn-primary btn-danger">Yes, delete
                    its
                </button>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- <script src="js/bootstrap.js"></script> -->
<script src="js/bootstrap-3.3.7.js"></script>
<script src="js/script.js"></script>
</body>
</html>